<?php

namespace App\Libs\Progressions;

class FibonacciSequence {
    private int $first;
    private int $second ;
    private float $size;

    public function __construct(float $size, int $first = 0, int $second = 1) {
        $this->size = $size;
        $this->first = $first;
        $this->second = $second;
    }
    
    /**
     * @return array<float>
     */
    public function generate(): array {
        $result = [$this->first, $this->second];
        for ($i = 2; $i < $this->size; $i++) {
            $result[] = $this->formCalc($this->first, $this->second);
        }
        return $result;
    }

    public function formCalc(int $first, int $second): float {
        $next = $first + $second;

        $this->first = $this->second;
        $this->second = $next;
        return $next;
    }
}