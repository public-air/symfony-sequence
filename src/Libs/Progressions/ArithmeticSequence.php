<?php

namespace App\Libs\Progressions;

class ArithmeticSequence {
    private float $start;
    private float $increment;
    private int $size;

    public function __construct(float $start, float $increment, int $size) {
        $this->start = $start;
        $this->increment = $increment;
        $this->size = $size;
    }
    /**
     * @return array<float>
     */
    public function generate(): array {
        $result = [];
        for ($i = 0; $i < $this->size; $i++) {
            $result[] = $this->formCalc($this->start, $i, $this->increment);
        }
        return $result;
    }

    public function formCalc(float $a, float $n, float $increment): float {
        return $a + ($n * $increment);
    }
}