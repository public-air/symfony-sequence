<?php

namespace App\Libs\Progressions;
class GeometricSequence {
    private float $start;
    private float $ratio;
    private int $size;

    public function __construct(float $start, float $ratio, int $size) {
        $this->start = $start;
        $this->ratio = $ratio;
        $this->size = $size;
    }

    /**
     * @return array<float>
     */
    public function generate(): array {
        $result = array();
        for ($i = 0; $i < $this->size; $i++) {
            $result[] = $this->formCalc($this->start, $this->ratio, $i);
        }
        return $result;
    }

    /**
     * a, ar, ar^2, ar^3, ar^4, ...
     */
    public function formCalc(float $a, float $ratio, int $n): float {
        return $a * pow($ratio, $n);
    }
}