<?php

namespace App\Controller;

use App\Libs\Progressions\ArithmeticSequence;
use App\Libs\Progressions\FibonacciSequence;
use App\Libs\Progressions\GeometricSequence;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SequenceController extends AbstractController
{
    /**
     * https://www.cuemath.com/algebra/arithmetic-progressions/
     * start: 1, increment: 1, size: 5
     * 1,2,3,4,5
     */
    #[Route('/arithmetic', name: 'arithmetic')]
    public function arithmetic(Request $request): JsonResponse
    {

        $start = $request->get('start');
        if (!is_numeric($start)) {
            throw new \InvalidArgumentException('start must be a numeric value');
        }

        $start = (float) $start;

        $increment = $request->get('increment');
        if (!is_numeric($increment)) {
            throw new \InvalidArgumentException('Increment must be a numeric value');
        }

        $increment = (float) $increment;

        $size = $request->get('size');
        if (!is_numeric($size)) {
            throw new \InvalidArgumentException('size must be a numeric value');
        }
        
        $size = (int) $size;

        $arithmetic = new ArithmeticSequence($start, $increment, $size);
        $result = $arithmetic->generate();

        return $this->json([
            'start' => $start,
            'increment' => $increment,
            'size' => $size,
            'result' => $result
        ]);
    }

    #[Route('/geometric', name: 'geometric')]
    public function geometric(Request $request): JsonResponse
    {

        $start = $request->get('start', 1);
        if (!is_numeric($start)) {
            throw new \InvalidArgumentException('start must be a numeric value');
        }

        $start = (float) $start;

        $ratio = $request->get('ratio', 1);
        if (!is_numeric($ratio)) {
            throw new \InvalidArgumentException('ratio must be a numeric value');
        }

        $ratio = (float) $ratio;

        $size = $request->get('size', 5);
        if (!is_numeric($size)) {
            throw new \InvalidArgumentException('size must be a numeric value');
        }
        
        $size = (int) $size;

        $sequence = new GeometricSequence($start, $ratio, $size);
        $result = $sequence->generate();

        return $this->json([
            'start' => $start,
            'ratio' => $ratio,
            'size' => $size,
            'result' => $result
        ]);
    }


    #[Route('/fibonacci', name: 'fibonacci')]
    public function fibonacci(Request $request): JsonResponse
    {
        $start = $request->get('start', 1);
        if (!is_numeric($start)) {
            throw new \InvalidArgumentException('start must be a numeric value');
        }

        $start = (float) $start;

        $size = $request->get('size', 5);
        if (!is_numeric($size)) {
            throw new \InvalidArgumentException('size must be a numeric value');
        }
        
        $size = (int) $size;

        $sequence = new FibonacciSequence($size);
        $result = $sequence->generate();

        return $this->json([
            'start' => $start,
            'size' => $size,
            'result' => $result
        ]);
    }

}
