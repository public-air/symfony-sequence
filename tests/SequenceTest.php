<?php

namespace App\Tests;

use App\Libs\Progressions\ArithmeticSequence;
use App\Libs\Progressions\FibonacciSequence;
use App\Libs\Progressions\GeometricSequence;
use PHPUnit\Framework\TestCase;

class SequenceTest extends TestCase
{
    public function testArithmetic(): void
    {
        $arithmetricSequence = new ArithmeticSequence(1, 5, 5);
        $result = $arithmetricSequence->generate();
        $this->assertEquals(implode(',', $result), '1,6,11,16,21');

        $arithmetricSequence = new ArithmeticSequence(5, 2, 5);
        $result = $arithmetricSequence->generate();
        $this->assertEquals(implode(',', $result), '5,7,9,11,13');

        $arithmetricSequence = new ArithmeticSequence(5, 3, 5);
        $result = $arithmetricSequence->generate();
        $this->assertEquals(implode(',', $result), '5,8,11,14,17');
    }

    public function testGeometric(): void
    {
        $geometricSequence = new GeometricSequence(100, 0.5, 5);
        $result = $geometricSequence->generate();
        $this->assertEquals(implode(',', $result), '100,50,25,12.5,6.25');

        $geometricSequence = new GeometricSequence(10, 0.5, 5);
        $result = $geometricSequence->generate();
        $this->assertEquals(implode(',', $result), '10,5,2.5,1.25,0.625');

        $geometricSequence = new GeometricSequence(2, 3, 5);
        $result = $geometricSequence->generate();
        $this->assertEquals(implode(',', $result), '2,6,18,54,162');
    }

    public function testFibonacci(): void
    {
        $fibonacciSequence = new FibonacciSequence(10);
        $result = $fibonacciSequence->generate();
        $this->assertEquals(implode(',', $result), '0,1,1,2,3,5,8,13,21,34');
    }
}
