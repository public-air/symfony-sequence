# Programming task

As a Software Team, we would like to see some of your work. In the following you'll find a short task, on which you can work on your own and share your solution with us. If you have any questions, don't hesitate to ask your interviewer.

## Create a backend with `symfony`

Create a Symfony-App with a REST-API to fetch the sequence for different progressions

-   Arithmetic
-   Geometric

Bonus

-   Fibonacci

The client states, for example the start, increment or ratio and size of the sequence. Examples:

-   _Arithmetic_: 1,2,3,4,5

    -   _start_: 1
    -   _increment_: 1
    -   _size_: 5

-   _Geometric_: 100,50,25,12.5,6.25

    -   _start_: 100
    -   _ratio_: 0.5
    -   _size_: 5

-   _Fibonacci_: 0,1,1,2,3,5,8,13,21,34
    -   _size_: 10

The API returns the corresponding sequence as an array.

Please use a current `symfony-` and `php-version` as well as `git` to perform source code management;
in the end we would like to get access to your git-repository to see your solution.

-   You can use docker if you like
-   You can use whatever you like: specific bundles or components :-)
-   You can add more or less functionality, it's about the way you do it - hopefully you have some fun! :-)

## More information about progressions

### What is a progression?

_Wikipedia tells us:_

A succession of quantities in which there is a constant relation between each member and the one succeeding it.

#### Arithmetic-Progression

An arithmetic progression or arithmetic sequence is a sequence of numbers such that the difference from any succeeding term to its preceding term remains constant throughout the sequence.The constant difference is called common difference of that arithmetic progression. For instance, the sequence 5, 7, 9, 11, 13, 15, . . . is an arithmetic progression with a common difference of 2.

#### Geometric-Progression

In mathematics, a geometric progression, also known as a geometric sequence, is a sequence of non-zero numbers where each term after the first is found by multiplying the previous one by a fixed, non-zero number called the common ratio. For example, the sequence 2, 6, 18, 54, ... is a geometric progression with common ratio 3. Similarly 10, 5, 2.5, 1.25, ... is a geometric sequence with common ratio 1/2.

#### Fibonacci-Progression

In mathematics, the Fibonacci numbers, commonly denoted Fn , form a sequence, the Fibonacci sequence, in which each number is the sum of the two preceding ones. The sequence commonly starts from 0 and 1, although some authors start the sequence from 1 and 1 or sometimes (as did Fibonacci) from 1 and 2. Starting from 0 and 1, the first few values in the sequence are: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144.
